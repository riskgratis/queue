# Copyright 2016-2019 Camptocamp SA
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html)

from odoo.tests import common
<<<<<<< HEAD

=======
>>>>>>> upstream/12.0
from odoo.addons.queue_job.job import Job


class JobCommonCase(common.TransactionCase):
<<<<<<< HEAD
    def setUp(self):
        super().setUp()
        self.queue_job = self.env["queue.job"]
        self.user = self.env["res.users"]
        self.method = self.env["test.queue.job"].testing_method
=======

    def setUp(self):
        super().setUp()
        self.queue_job = self.env['queue.job']
        self.user = self.env['res.users']
        self.method = self.env['test.queue.job'].testing_method
        self.env['queue.job.function']._register_job(
            self.env['test.queue.job'],
            self.method
        )
>>>>>>> upstream/12.0

    def _create_job(self):
        test_job = Job(self.method)
        test_job.store()
        stored = Job.db_record_from_uuid(self.env, test_job.uuid)
        self.assertEqual(len(stored), 1)
        return stored
