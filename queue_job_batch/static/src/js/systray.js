<<<<<<< HEAD
<<<<<<< HEAD
odoo.define("queue_job_batch.systray", function (require) {
    "use strict";

    var core = require("web.core");
    var session = require("web.session");
    var SystrayMenu = require("web.SystrayMenu");
    var Widget = require("web.Widget");
    require("bus.BusService");
=======
=======
>>>>>>> upstream/11.0
odoo.define('queue_job_batch.systray', function (require) {
    "use strict";

    var core = require('web.core');
    var session = require('web.session');
    var SystrayMenu = require('web.SystrayMenu');
    var Widget = require('web.Widget');
<<<<<<< HEAD
    var BusService = require('bus.BusService');
>>>>>>> upstream/12.0
=======
    var bus = require('bus.bus').bus;
>>>>>>> upstream/11.0

    var QWeb = core.qweb;

    var QueueJobBatchMenu = Widget.extend({
<<<<<<< HEAD
<<<<<<< HEAD
        template: "queue_job_batch.view.Menu",
        events: {
            click: "_onMenuClick",
=======
        template:'queue_job_batch.view.Menu',
        events: {
            "click": "_onMenuClick",
>>>>>>> upstream/12.0
            "click .o_mail_preview": "_onQueueJobBatchClick",
=======
        template:'queue_job_batch.view.Menu',
        events: {
            "click": "_onMenuClick",
            "click .o_mail_channel_preview": "_onQueueJobBatchClick",
>>>>>>> upstream/11.0
            "click .o_view_all_batch_jobs": "_viewAllQueueJobBatches",
            "click .o_queue_job_batch_hide": "_hideJobBatchClick",
        },
        renderElement: function () {
            this._super();
            var self = this;
<<<<<<< HEAD
<<<<<<< HEAD
            session
                .user_has_group("queue_job_batch.group_queue_job_batch_user")
                .then(function (data) {
                    self.manager = data;
                    if (data) {
                        self.do_show();
                    }
                });
        },
        start: function () {
            var self = this;
            session
                .user_has_group("queue_job_batch.group_queue_job_batch_user")
                .then(function (data) {
                    self.manager = data;
                    if (data) {
                        self.$queue_job_batch_preview = self.$(
                            ".o_mail_systray_dropdown_items"
                        );
                        self._updateQueueJobBatchesPreview();
                        var channel = "queue.job.batch";
                        self.call("bus_service", "addChannel", channel);
                        self.call("bus_service", "startPolling");
                        self.call(
                            "bus_service",
                            "onNotification",
                            self,
                            self._updateQueueJobBatchesPreview
                        );
                    }
                });
=======
=======
>>>>>>> upstream/11.0
            session.user_has_group(
                'queue_job_batch.group_queue_job_batch_user'
            ).then(function (data) {
                self.manager = data;
                if (data) {
                    self.do_show();
                }
            });
        },
        start: function () {
            var self = this;
            session.user_has_group(
                'queue_job_batch.group_queue_job_batch_user'
            ).then(function (data) {
                self.manager = data;
                if (data) {
                    self.$queue_job_batch_preview = self.$(
<<<<<<< HEAD
                        '.o_mail_systray_dropdown_items'
                    );
                    self._updateQueueJobBatchesPreview();
                    var channel = 'queue.job.batch';
                    self.call('bus_service', 'addChannel', channel);
                    self.call('bus_service', 'startPolling');
                    self.call(
                        'bus_service', 'onNotification',
                        self, self._updateQueueJobBatchesPreview,
                    );
                }
            });
>>>>>>> upstream/12.0
=======
                        '.o_mail_navbar_dropdown_channels'
                    );
                    self._updateQueueJobBatchesPreview();
                    var channel = 'queue.job.batch';
                    bus.add_channel(channel);
                    bus.on(
                        'notification',
                        self, self._updateQueueJobBatchesPreview
                    );
                }
            });
>>>>>>> upstream/11.0
            return this._super();
        },

        _getQueueJobBatchesData: function () {
            var self = this;

<<<<<<< HEAD
<<<<<<< HEAD
            return self
                ._rpc({
                    model: "queue.job.batch",
                    method: "search_read",
                    args: [
                        [
                            ["user_id", "=", session.uid],
                            "|",
                            ["state", "in", ["draft", "progress"]],
                            ["is_read", "=", false],
                        ],
                        [
                            "name",
                            "job_count",
                            "completeness",
                            "failed_percentage",
                            "finished_job_count",
                            "failed_job_count",
                            "state",
                        ],
                    ],
                    kwargs: {
                        context: session.user_context,
                    },
                })
                .then(function (data) {
                    self.job_batches = data;
                    self.jobBatchesCounter = data.length;
                    self.$(".o_notification_counter").text(self.jobBatchesCounter);
                    self.$el.toggleClass("o_no_notification", !self.jobBatchesCounter);
                });
        },

        _isOpen: function () {
            return this.$el.hasClass("open");
=======
=======
>>>>>>> upstream/11.0
            return self._rpc({
                model: 'queue.job.batch',
                method: 'search_read',
                args: [[
                    ['user_id', '=', session.uid],
                    '|', ['state', 'in', ['draft', 'progress']],
                    ['is_read', '=', false],
                ], [
                    'name', 'job_count', 'completeness', 'failed_percentage',
                    'finished_job_count', 'failed_job_count', 'state',
                ]],
                kwargs: {
                    context: session.user_context,
                },
            }).then(function (data) {
                self.job_batches = data;
                self.jobBatchesCounter = data.length;
                self.$('.o_notification_counter').text(self.jobBatchesCounter);
                self.$el.toggleClass(
                    'o_no_notification', !self.jobBatchesCounter
                );
            });
        },

        _isOpen: function () {
            return this.$el.hasClass('open');
<<<<<<< HEAD
>>>>>>> upstream/12.0
=======
>>>>>>> upstream/11.0
        },

        _updateQueueJobBatchesPreview: function () {
            var self = this;
            self._getQueueJobBatchesData().then(function () {
<<<<<<< HEAD
<<<<<<< HEAD
                self.$queue_job_batch_preview.html(
                    QWeb.render("queue_job_batch.view.Data", {
                        job_batches: self.job_batches,
                    })
                );
            });
        },
        _hideJobBatchClick: function (event) {
            // Hide the batch without navigating to it.
            event.preventDefault();
            event.stopPropagation();
            var queue_job_batch_id = parseInt(
                $(event.currentTarget, 10).data("job-batch-id"),
                10
            );
=======
=======
>>>>>>> upstream/11.0
                self.$queue_job_batch_preview.html(QWeb.render(
                    'queue_job_batch.view.Data', {
                        job_batches : self.job_batches,
                    }
                ));
            });
        },
        _hideJobBatchClick: function (event) {
<<<<<<< HEAD
            // hide the batch without navigating to it.
            event.preventDefault();
            event.stopPropagation();
            var queue_job_batch_id = parseInt(
                $(event.currentTarget, 10).data('job-batch-id'), 10);
>>>>>>> upstream/12.0
=======
            var queue_job_batch_id = parseInt(
                $(event.currentTarget, 10).data('job-batch-id'), 10);
>>>>>>> upstream/11.0
            this._hideJobBatch(event, queue_job_batch_id);
        },
        _hideJobBatch: function (event, queue_job_batch_id) {
            this._rpc({
<<<<<<< HEAD
<<<<<<< HEAD
                model: "queue.job.batch",
                method: "set_read",
                args: [queue_job_batch_id],
=======
                model: 'queue.job.batch',
                method: 'set_read',
                args: [queue_job_batch_id,],
>>>>>>> upstream/12.0
=======
                model: 'queue.job.batch',
                method: 'set_read',
                args: [[queue_job_batch_id]],
>>>>>>> upstream/11.0
                kwargs: {
                    context: session.user_context,
                },
            });
        },
        _onQueueJobBatchClick: function (event) {
            var queue_job_batch_id = parseInt(
<<<<<<< HEAD
<<<<<<< HEAD
                $(event.currentTarget, 10).data("job-batch-id"),
                10
            );
            this._hideJobBatch(event, queue_job_batch_id);
            this.do_action({
                type: "ir.actions.act_window",
                name: "Job batches",
                res_model: "queue.job.batch",
                views: [[false, "form"]],
=======
=======
>>>>>>> upstream/11.0
                $(event.currentTarget, 10).data('job-batch-id'), 10);
            this._hideJobBatch(event, queue_job_batch_id);
            this.do_action({
                type: 'ir.actions.act_window',
                name: 'Job batches',
                res_model: 'queue.job.batch',
                views: [[false, 'form']],
<<<<<<< HEAD
>>>>>>> upstream/12.0
=======
>>>>>>> upstream/11.0
                res_id: queue_job_batch_id,
            });
        },
        _viewAllQueueJobBatches: function () {
<<<<<<< HEAD
<<<<<<< HEAD
            this.do_action("queue_job_batch.action_view_your_queue_job_batch");
=======
            this.do_action(
                'queue_job_batch.action_view_your_queue_job_batch');
>>>>>>> upstream/12.0
=======
            this.do_action(
                'queue_job_batch.action_view_your_queue_job_batch');
>>>>>>> upstream/11.0
        },
        _onMenuClick: function () {
            if (!this._isOpen()) {
                this._updateQueueJobBatchesPreview();
            }
        },
<<<<<<< HEAD
<<<<<<< HEAD
=======

>>>>>>> upstream/12.0
=======

>>>>>>> upstream/11.0
    });

    SystrayMenu.Items.push(QueueJobBatchMenu);

<<<<<<< HEAD
    return QueueJobBatchMenu;
=======
    return {
        QueueJobBatchMenu: QueueJobBatchMenu,
    };
>>>>>>> upstream/11.0
});
