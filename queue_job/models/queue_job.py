<<<<<<< HEAD
# Copyright 2013-2020 Camptocamp SA
=======
# Copyright 2013-2016 Camptocamp SA
<<<<<<< HEAD
<<<<<<< HEAD
>>>>>>> upstream/12.0
=======
>>>>>>> upstream/11.0
=======
>>>>>>> upstream/10.0
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html)

import ast
import logging
import random
<<<<<<< HEAD
from datetime import datetime, timedelta

<<<<<<< HEAD
from odoo import _, api, exceptions, fields, models
from odoo.osv import expression
from odoo.tools import html_escape

=======
import re
from collections import namedtuple
from datetime import datetime, timedelta

from odoo import _, api, exceptions, fields, models, tools
>>>>>>> upstream/12.0
=======
from odoo import models, fields, api, exceptions, _
from odoo.osv import expression
<<<<<<< HEAD
# Import `Serialized` field straight to avoid:
# * remember to use --load=base_sparse_field...
# * make pytest happy
# * make everybody happy :
>>>>>>> upstream/11.0
from odoo.addons.base_sparse_field.models.fields import Serialized
from odoo.osv import expression
from odoo.tools import html_escape
=======
>>>>>>> upstream/10.0

from ..delay import Graph
from ..exception import JobError
from ..fields import JobSerialized
from ..job import (
    CANCELLED,
    DONE,
    FAILED,
<<<<<<< HEAD
    PENDING,
    STARTED,
    STATES,
    WAIT_DEPENDENCIES,
    Job,
=======
    Job,
    PENDING,
    STARTED,
    STATES,
    WAIT_DEPENDENCIES
>>>>>>> upstream/12.0
)

_logger = logging.getLogger(__name__)


<<<<<<< HEAD
=======
regex_job_function_name = re.compile(r"^<([0-9a-z_\.]+)>\.([0-9a-zA-Z_]+)$")


>>>>>>> upstream/12.0
class QueueJob(models.Model):
    """Model storing the jobs to be executed."""

    _name = "queue.job"
    _description = "Queue Job"
    _inherit = ["mail.thread", "mail.activity.mixin"]
    _log_access = False

    _order = "date_created DESC, date_done DESC"

    _removal_interval = 30  # days
<<<<<<< HEAD
<<<<<<< HEAD
    _default_related_action = "related_action_open_record"
=======
    _default_related_action = 'related_action_open_record'
>>>>>>> upstream/12.0
=======
    _default_related_action = 'related_action_open_record'
>>>>>>> upstream/10.0

    # This must be passed in a context key "_job_edit_sentinel" to write on
    # protected fields. It protects against crafting "queue.job" records from
    # RPC (e.g. on internal methods). When ``with_delay`` is used, the sentinel
    # is set.
    EDIT_SENTINEL = object()
    _protected_fields = (
        "uuid",
        "name",
        "date_created",
        "model_name",
        "method_name",
        "func_string",
        "channel_method_name",
        "job_function_id",
        "records",
        "args",
        "kwargs",
    )

<<<<<<< HEAD
    uuid = fields.Char(string="UUID", readonly=True, index=True, required=True)
<<<<<<< HEAD
=======
    graph_uuid = fields.Char(
        string="Graph UUID",
        readonly=True,
        index=True,
        help="Single shared identifier of a Graph. Empty for a single job.",
    )
>>>>>>> upstream/14.0
    user_id = fields.Many2one(comodel_name="res.users", string="User ID")
    company_id = fields.Many2one(
        comodel_name="res.company", string="Company", index=True
    )
    name = fields.Char(string="Description", readonly=True)

    model_name = fields.Char(string="Model", readonly=True)
=======
    uuid = fields.Char(string='UUID',
                       readonly=True,
                       index=True,
                       required=True)
    graph_uuid = fields.Char(
        string='Graph UUID',
        readonly=True,
        index=True,
        help="Single shared identifier of a Graph. Empty for a single job."
    )
    user_id = fields.Many2one(comodel_name='res.users',
                              string='User ID')
    company_id = fields.Many2one(comodel_name='res.company',
                                 string='Company', index=True)
    name = fields.Char(string='Description', readonly=True)

    model_name = fields.Char(string='Model', readonly=True)
>>>>>>> upstream/12.0
    method_name = fields.Char(readonly=True)
    # record_ids field is only for backward compatibility (e.g. used in related
    # actions), can be removed (replaced by "records") in 14.0
    record_ids = JobSerialized(compute="_compute_record_ids", base_type=list)
    records = JobSerialized(
<<<<<<< HEAD
        string="Record(s)",
        readonly=True,
        base_type=models.BaseModel,
    )
    dependencies = Serialized(readonly=True)
    # dependency graph as expected by the field widget
    dependency_graph = Serialized(compute="_compute_dependency_graph")
=======
        string="Record(s)", readonly=True, base_type=models.BaseModel,
    )
    dependencies = Serialized(readonly=True)
    # dependency graph as expected by the field widget
    dependency_graph = Serialized(compute='_compute_dependency_graph')
>>>>>>> upstream/12.0
    graph_jobs_count = fields.Integer(compute="_compute_graph_jobs_count")
    args = JobSerialized(readonly=True, base_type=tuple)
    kwargs = JobSerialized(readonly=True, base_type=dict)
    func_string = fields.Char(string="Task", readonly=True)

<<<<<<< HEAD
    state = fields.Selection(STATES, readonly=True, required=True, index=True)
    priority = fields.Integer()
    exc_name = fields.Char(string="Exception", readonly=True)
    exc_message = fields.Char(string="Exception Message", readonly=True)
    exc_info = fields.Text(string="Exception Info", readonly=True)
=======
    state = fields.Selection(STATES,
                             readonly=True,
                             required=True,
                             index=True)
    priority = fields.Integer()
    exc_name = fields.Char(string="Exception", readonly=True)
    exc_message = fields.Char(string="Exception Message", readonly=True)
    exc_info = fields.Text(string='Exception Info', readonly=True)
>>>>>>> upstream/12.0
    result = fields.Text(readonly=True)

    date_created = fields.Datetime(string="Created Date", readonly=True)
    date_started = fields.Datetime(string="Start Date", readonly=True)
    date_enqueued = fields.Datetime(string="Enqueue Time", readonly=True)
    date_done = fields.Datetime(readonly=True)
    exec_time = fields.Float(
        string="Execution Time (avg)",
        group_operator="avg",
        help="Time required to execute this job in seconds. Average when grouped.",
    )
<<<<<<< HEAD
<<<<<<< HEAD
=======
    date_cancelled = fields.Datetime(readonly=True)
>>>>>>> upstream/14.0
=======
    date_cancelled = fields.Datetime(readonly=True)
>>>>>>> upstream/12.0

    eta = fields.Datetime(string="Execute only after")
    retry = fields.Integer(string="Current try")
    max_retries = fields.Integer(
        string="Max. retries",
        help="The job will fail if the number of tries reach the "
        "max. retries.\n"
        "Retries are infinite when empty.",
    )
    # FIXME the name of this field is very confusing
<<<<<<< HEAD
    channel_method_name = fields.Char(readonly=True)
    job_function_id = fields.Many2one(
        comodel_name="queue.job.function", string="Job Function", readonly=True,
=======
    channel_method_name = fields.Char(string="Complete Method Name", readonly=True)
    job_function_id = fields.Many2one(
        comodel_name="queue.job.function",
        string="Job Function",
        readonly=True,
>>>>>>> upstream/14.0
    )

<<<<<<< HEAD
=======
    # FIXME the name of this field is very confusing
    channel_method_name = fields.Char(string="Complete Method Name", readonly=True)
    job_function_id = fields.Many2one(comodel_name='queue.job.function',
                                      string='Job Function',
                                      readonly=True)

>>>>>>> upstream/12.0
    channel = fields.Char(index=True)

<<<<<<< HEAD
    identity_key = fields.Char(readonly=True)
    worker_pid = fields.Integer(readonly=True)
=======
    identity_key = fields.Char()

    @api.model_cr
    def init(self):
        self._cr.execute(
            'SELECT indexname FROM pg_indexes WHERE indexname = %s ',
            ('queue_job_identity_key_state_partial_index',)
        )
        if not self._cr.fetchone():
            self._cr.execute(
                "CREATE INDEX queue_job_identity_key_state_partial_index "
                "ON queue_job (identity_key) WHERE state in ('pending', "
                "'enqueued') AND identity_key IS NOT NULL;"
            )

    identity_key = fields.Char()

    @api.model_cr
    def init(self):
        self._cr.execute(
            'SELECT indexname FROM pg_indexes WHERE indexname = %s ',
            ('queue_job_identity_key_state_partial_index',)
        )
        if not self._cr.fetchone():
            self._cr.execute(
                "CREATE INDEX queue_job_identity_key_state_partial_index "
                "ON queue_job (identity_key) WHERE state in ('pending', "
                "'enqueued') AND identity_key IS NOT NULL;"
            )

    @api.multi
    def _inverse_channel(self):
        self.filtered(lambda a: not a.channel)._compute_channel()
>>>>>>> upstream/11.0

<<<<<<< HEAD
=======
    @api.model_cr
>>>>>>> upstream/12.0
    def init(self):
        self._cr.execute(
            "SELECT indexname FROM pg_indexes WHERE indexname = %s ",
            ("queue_job_identity_key_state_partial_index",),
        )
        if not self._cr.fetchone():
            self._cr.execute(
                "CREATE INDEX queue_job_identity_key_state_partial_index "
                "ON queue_job (identity_key) WHERE state in ('pending', "
                "'enqueued') AND identity_key IS NOT NULL;"
            )

    @api.depends("records")
    def _compute_record_ids(self):
        for record in self:
            record.record_ids = record.records.ids

<<<<<<< HEAD
<<<<<<< HEAD
=======
    @api.depends("dependencies")
=======
    @api.depends('dependencies')
>>>>>>> upstream/12.0
    def _compute_dependency_graph(self):
        graph_uuids = [uuid for uuid in self.mapped("graph_uuid") if uuid]
        jobs_groups = self.env["queue.job"].read_group(
            [("graph_uuid", "in", graph_uuids)],
            ["graph_uuid", "ids:array_agg(id)"],
<<<<<<< HEAD
            ["graph_uuid"],
=======
            ["graph_uuid"]
>>>>>>> upstream/12.0
        )
        ids_per_graph_uuid = {
            group["graph_uuid"]: group["ids"] for group in jobs_groups
        }
        for record in self:
<<<<<<< HEAD
            if not record.graph_uuid:
                record.dependency_graph = {}
                continue

            graph_jobs = self.browse(ids_per_graph_uuid.get(record.graph_uuid) or [])
            if not graph_jobs:
                record.dependency_graph = {}
                continue

<<<<<<< HEAD
            graph_ids = {graph_job.uuid: graph_job.id for graph_job in graph_jobs}
            graph_jobs_by_ids = {graph_job.id: graph_job for graph_job in graph_jobs}
=======
            graph_ids = {
                graph_job.uuid: graph_job.id for graph_job in graph_jobs
            }
            graph_jobs_by_ids = {
                graph_job.id: graph_job for graph_job in graph_jobs
            }
>>>>>>> upstream/12.0

            graph = Graph()
            for graph_job in graph_jobs:
                graph.add_vertex(graph_job.id)
<<<<<<< HEAD
                for parent_uuid in graph_job.dependencies["depends_on"]:
=======
                for parent_uuid in graph_job.dependencies['depends_on']:
>>>>>>> upstream/12.0
                    parent_id = graph_ids.get(parent_uuid)
                    if not parent_id:
                        continue
                    graph.add_edge(parent_id, graph_job.id)
<<<<<<< HEAD
                for child_uuid in graph_job.dependencies["reverse_depends_on"]:
=======
                for child_uuid in graph_job.dependencies['reverse_depends_on']:
>>>>>>> upstream/12.0
                    child_id = graph_ids.get(child_uuid)
                    if not child_id:
                        continue
                    graph.add_edge(graph_job.id, child_id)
=======
            model = self.env[record.model_name]
            method = getattr(model, record.method_name)
            channel_method_name = channel_func_name(model, method)
            func_model = self.env['queue.job.function']
            function = func_model.search([('name', '=', channel_method_name)], limit=1)
            record.channel_method_name = channel_method_name
            record.job_function_id = function
>>>>>>> upstream/11.0

            record.dependency_graph = {
                # list of ids
<<<<<<< HEAD
                "nodes": [
=======
                'nodes': [
>>>>>>> upstream/12.0
                    graph_jobs_by_ids[graph_id]._dependency_graph_vis_node()
                    for graph_id in graph.vertices()
                ],
                # list of tuples (from, to)
<<<<<<< HEAD
                "edges": graph.edges(),
=======
                'edges': graph.edges(),
>>>>>>> upstream/12.0
            }

    def _dependency_graph_vis_node(self):
        """Return the node as expected by the JobDirectedGraph widget"""
        default = ("#D2E5FF", "#2B7CE9")
        colors = {
            DONE: ("#C2FABC", "#4AD63A"),
            FAILED: ("#FB7E81", "#FA0A10"),
            STARTED: ("#FFFF00", "#FFA500"),
        }
        return {
            "id": self.id,
<<<<<<< HEAD
            "title": "<strong>%s</strong><br/>%s"
            % (
=======
            "title": "<strong>%s</strong><br/>%s" % (
>>>>>>> upstream/12.0
                html_escape(self.display_name),
                html_escape(self.func_string),
            ),
            "color": colors.get(self.state, default)[0],
            "border": colors.get(self.state, default)[1],
            "shadow": True,
        }

<<<<<<< HEAD
    def _compute_graph_jobs_count(self):
        graph_uuids = [uuid for uuid in self.mapped("graph_uuid") if uuid]
        jobs_groups = self.env["queue.job"].read_group(
            [("graph_uuid", "in", graph_uuids)], ["graph_uuid"], ["graph_uuid"]
        )
        count_per_graph_uuid = {
            group["graph_uuid"]: group["graph_uuid_count"] for group in jobs_groups
=======
    @api.multi
    def _compute_graph_jobs_count(self):
        graph_uuids = [uuid for uuid in self.mapped("graph_uuid") if uuid]
        jobs_groups = self.env["queue.job"].read_group(
            [("graph_uuid", "in", graph_uuids)],
            ["graph_uuid"],
            ["graph_uuid"]
        )
        count_per_graph_uuid = {
            group["graph_uuid"]: group["graph_uuid_count"]
            for group in jobs_groups
>>>>>>> upstream/12.0
        }
        for record in self:
            record.graph_jobs_count = count_per_graph_uuid.get(record.graph_uuid) or 0

<<<<<<< HEAD
>>>>>>> upstream/14.0
=======
>>>>>>> upstream/12.0
    @api.model_create_multi
    def create(self, vals_list):
        if self.env.context.get("_job_edit_sentinel") is not self.EDIT_SENTINEL:
            # Prevent to create a queue.job record "raw" from RPC.
            # ``with_delay()`` must be used.
            raise exceptions.AccessError(
                _("Queue jobs must be created by calling 'with_delay()'.")
            )
        return super(
            QueueJob,
            self.with_context(mail_create_nolog=True, mail_create_nosubscribe=True),
        ).create(vals_list)

    def write(self, vals):
        if self.env.context.get("_job_edit_sentinel") is not self.EDIT_SENTINEL:
            write_on_protected_fields = [
                fieldname for fieldname in vals if fieldname in self._protected_fields
            ]
            if write_on_protected_fields:
                raise exceptions.AccessError(
                    _("Not allowed to change field(s): {}").format(
                        write_on_protected_fields
                    )
                )

        different_user_jobs = self.browse()
        if vals.get("user_id"):
            different_user_jobs = self.filtered(
                lambda records: records.env.user.id != vals["user_id"]
<<<<<<< HEAD
=======
            )

        if vals.get("state") == "failed":
            self._message_post_on_failure()

        result = super().write(vals)

        for record in different_user_jobs:
            # the user is stored in the env of the record, but we still want to
            # have a stored user_id field to be able to search/groupby, so
            # synchronize the env of records with user_id
            super(QueueJob, record).write(
                {"records": record.records.sudo(vals["user_id"])}
>>>>>>> upstream/12.0
            )
        return result

        if vals.get("state") == "failed":
            self._message_post_on_failure()

        result = super().write(vals)

        for record in different_user_jobs:
            # the user is stored in the env of the record, but we still want to
            # have a stored user_id field to be able to search/groupby, so
            # synchronize the env of records with user_id
            super(QueueJob, record).write(
                {"records": record.records.with_user(vals["user_id"])}
            )
        return result

    def open_related_action(self):
        """Open the related action associated to the job"""
        self.ensure_one()
        job = Job.load(self.env, self.uuid)
        action = job.related_action()
        if action is None:
<<<<<<< HEAD
            raise exceptions.UserError(_("No action available for this job"))
=======
            raise exceptions.UserError(_('No action available for this job'))
>>>>>>> upstream/10.0
        return action

    def open_graph_jobs(self):
        """Return action that opens all jobs of the same graph"""
        self.ensure_one()
        jobs = self.env["queue.job"].search([("graph_uuid", "=", self.graph_uuid)])

        action_jobs = self.env.ref("queue_job.action_queue_job")
        action = action_jobs.read()[0]
        action.update(
            {
                "name": _("Jobs for graph %s") % (self.graph_uuid),
                "context": {},
                "domain": [("id", "in", jobs.ids)],
            }
        )
        return action

<<<<<<< HEAD
=======
    @api.multi
    def open_graph_jobs(self):
        """Return action that opens all jobs of the same graph"""
        self.ensure_one()
        jobs = self.env["queue.job"].search([("graph_uuid", "=", self.graph_uuid)])

        action_jobs = self.env.ref('queue_job.action_queue_job')
        action = action_jobs.read()[0]
        action.update({
            "name": _("Jobs for graph %s") % (self.graph_uuid),
            "context": {},
            "domain": [("id", "in", jobs.ids)]
        })
        return action

    @api.multi
>>>>>>> upstream/12.0
    def _change_job_state(self, state, result=None):
        """Change the state of the `Job` object

        Changing the state of the Job will automatically change some fields
        (date, result, ...).
        """
        for record in self:
            job_ = Job.load(record.env, record.uuid)
            if state == DONE:
                job_.set_done(result=result)
                job_.store()
                job_.enqueue_waiting()
            elif state == PENDING:
                job_.set_pending(result=result)
                job_.store()
            elif state == CANCELLED:
                job_.set_cancelled(result=result)
                job_.store()
            else:
<<<<<<< HEAD
                raise ValueError("State not supported: %s" % state)
=======
                raise ValueError('State not supported: %s' % state)
>>>>>>> upstream/12.0

<<<<<<< HEAD
    def button_done(self):
        result = _("Manually set to done by %s") % self.env.user.name
        self._change_job_state(DONE, result=result)
        return True

<<<<<<< HEAD
    def button_cancelled(self):
        result = _("Cancelled by %s") % self.env.user.name
        self._change_job_state(CANCELLED, result=result)
        return True

<<<<<<< HEAD
=======
    @api.multi
    def action_done(self, reason=None):
        result = _(
            u"Manually set to done by {}"
        ).format(self.env.user.name)
        if reason:
            result = _(
                u"{} with reason: {}"
            ).format(result, reason)
        self._change_job_state(DONE, result=result)
        return True

    @api.multi
    def button_done(self):
        _logger.warning('deprecated, replaced by action_done()')
        return self.action_done()

    @api.multi
    def button_done_ask_reason(self):
        action = self.env.ref(
            'queue_job.action_set_jobs_done'
        ).read()[0]
        return action

    @api.multi
>>>>>>> upstream/10.0
    def requeue(self):
        jobs_to_requeue = self.filtered(lambda job_: job_.state != WAIT_DEPENDENCIES)
=======
    @api.multi
    def button_cancelled(self):
        result = _('Cancelled by %s') % self.env.user.name
        self._change_job_state(CANCELLED, result=result)
        return True
=======
    def _message_post_on_failure(self):
        # subscribe the users now to avoid to subscribe them
        # at every job creation
        domain = self._subscribe_users_domain()
        users = self.env['res.users'].search(domain)
        self.message_subscribe_users(user_ids=users.ids)
        for record in self:
            msg = record._message_failed_job()
            if msg:
                record.message_post(body=msg,
                                    subtype='queue_job.mt_job_failed')

    @api.multi
    def write(self, vals):
        res = super(QueueJob, self).write(vals)
        if vals.get('state') == 'failed':
            self._message_post_on_failure()
        return res
>>>>>>> upstream/11.0

    @api.multi
    def requeue(self):
        jobs_to_requeue = self.filtered(
            lambda job_: job_.state != WAIT_DEPENDENCIES
        )
>>>>>>> upstream/12.0
        jobs_to_requeue._change_job_state(PENDING)
        return True

    def _message_post_on_failure(self):
        # subscribe the users now to avoid to subscribe them
        # at every job creation
        domain = self._subscribe_users_domain()
        base_users = self.env["res.users"].search(domain)
        for record in self:
            users = base_users | record.user_id
            record.message_subscribe(partner_ids=users.mapped("partner_id").ids)
            msg = record._message_failed_job()
            if msg:
                record.message_post(body=msg, subtype_xmlid="queue_job.mt_job_failed")

    def _subscribe_users_domain(self):
        """Subscribe all users having the 'Queue Job Manager' group"""
        group = self.env.ref("queue_job.group_queue_job_manager")
        if not group:
            return None
<<<<<<< HEAD
        companies = self.mapped("company_id")
        domain = [("groups_id", "=", group.id)]
=======
        companies = self.mapped('company_id')
        domain = [('groups_id', '=', group.id)]
>>>>>>> upstream/11.0
        if companies:
            domain.append(("company_id", "in", companies.ids))
        return domain

    def _message_failed_job(self):
        """Return a message which will be posted on the job when it is failed.

        It can be inherited to allow more precise messages based on the
        exception informations.

        If nothing is returned, no message will be posted.
        """
        self.ensure_one()
        return _(
            "Something bad happened during the execution of the job. "
            "More details in the 'Exception Information' section."
        )

    def _needaction_domain_get(self):
        """Returns the domain to filter records that require an action

        :return: domain or False is no action
        """
        return [("state", "=", "failed")]

    def autovacuum(self):
<<<<<<< HEAD
<<<<<<< HEAD
        """Delete all jobs done based on the removal interval defined on the
           channel

        Called from a cron.
        """
<<<<<<< HEAD
        for channel in self.env["queue.job.channel"].search([]):
            deadline = datetime.now() - timedelta(days=int(channel.removal_interval))
            while True:
                jobs = self.search(
                    [
<<<<<<< HEAD
                        ("date_done", "<=", deadline),
=======
                        "|",
                        ("date_done", "<=", deadline),
                        ("date_cancelled", "<=", deadline),
>>>>>>> upstream/14.0
                        ("channel", "=", channel.complete_name),
                    ],
                    limit=1000,
                )
                if jobs:
                    jobs.unlink()
<<<<<<< HEAD
                    self.env.cr.commit()  # pylint: disable=E8102
=======
>>>>>>> upstream/14.0
                else:
                    break
        return True

    def requeue_stuck_jobs(self, enqueued_delta=5, started_delta=0):
        """Fix jobs that are in a bad states
=======
        """ Delete all jobs done based on the removal interval defined on the
            channel

        Called from a cron.
        """
        for channel in self.env['queue.job.channel'].search([]):
            deadline = datetime.now() - timedelta(
                days=int(channel.removal_interval))
            jobs = self.search(
                [('date_done', '<=', fields.Datetime.to_string(deadline)),
                 ('channel', '=', channel.complete_name)],
            )
            if jobs:
                jobs.unlink()
        return True

    @api.model
    def requeue_stuck_jobs(self, enqueued_delta=5, started_delta=0):
        """Fix jobs that are in a bad states
        :param in_queue_delta: lookup time in minutes for jobs
                                that are in enqueued state

        :param started_delta: lookup time in minutes for jobs
                                that are in enqueued state,
                                0 means that it is not checked
        """
        self._get_stuck_jobs_to_requeue(
            enqueued_delta=enqueued_delta,
            started_delta=started_delta
        ).requeue()
        return True

    @api.model
    def _get_stuck_jobs_domain(self, queue_dl, started_dl):
        domain = []
        now = fields.datetime.now()
        if queue_dl:
            queue_dl = now - timedelta(minutes=queue_dl)
            domain.append([
                '&',
                ('date_enqueued', '<=', fields.Datetime.to_string(queue_dl)),
                ('state', '=', 'enqueued'),
            ])
        if started_dl:
            started_dl = now - timedelta(minutes=started_dl)
            domain.append([
                '&',
                ('date_started', '<=', fields.Datetime.to_string(started_dl)),
                ('state', '=', 'started'),
            ])
        if not domain:
            raise exceptions.ValidationError(
                _("If both parameters are 0, ALL jobs will be requeued!")
            )
        return expression.OR(domain)

    @api.model
    def _get_stuck_jobs_to_requeue(self, enqueued_delta, started_delta):
        job_model = self.env['queue.job']
        stuck_jobs = job_model.search(self._get_stuck_jobs_domain(
            enqueued_delta,
            started_delta,
        ))
        return stuck_jobs

    @api.multi
    def related_action_open_record(self):
        """Open a form view with the record(s) of the job.

        For instance, for a job on a ``product.product``, it will open a
        ``product.product`` form view with the product record(s) concerned by
        the job. If the job concerns more than one record, it opens them in a
        list.

        This is the default related action.

        """
        self.ensure_one()
        model_name = self.model_name
        records = self.env[model_name].browse(self.record_ids).exists()
        if not records:
            return None
        action = {
            'name': _('Related Record'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': records._name,
        }
        if len(records) == 1:
            action['res_id'] = records.id
        else:
            action.update({
                'name': _('Related Records'),
                'view_mode': 'tree,form',
                'domain': [('id', 'in', records.ids)],
            })
        return action

>>>>>>> upstream/10.0

=======
        """ Delete all jobs done based on the removal interval defined on the
            channel

        Called from a cron.
        """
        for channel in self.env['queue.job.channel'].search([]):
            deadline = datetime.now() - timedelta(
                days=int(channel.removal_interval))
            jobs = self.search(
                [('date_done', '<=', fields.Datetime.to_string(deadline)),
                 ('channel', '=', channel.complete_name)],
            )
            if jobs:
                jobs.unlink()
        return True

    @api.model
    def requeue_stuck_jobs(self, enqueued_delta=5, started_delta=0):
        """Fix jobs that are in a bad states
>>>>>>> upstream/11.0
        :param in_queue_delta: lookup time in minutes for jobs
                                that are in enqueued state

        :param started_delta: lookup time in minutes for jobs
                                that are in enqueued state,
                                0 means that it is not checked
        """
        self._get_stuck_jobs_to_requeue(
<<<<<<< HEAD
            enqueued_delta=enqueued_delta, started_delta=started_delta
        ).requeue()
        return True

=======
        for channel in self.env['queue.job.channel'].search([]):
            deadline = datetime.now() - timedelta(
                days=int(channel.removal_interval))
            jobs = self.search(
                ['|',
                 ('date_done', '<=', deadline),
                 ('date_cancelled', '<=', deadline),
                 ('channel', '=', channel.complete_name)],
            )
            if jobs:
                jobs.unlink()
        return True

    @api.model
    def requeue_stuck_jobs(self, enqueued_delta=5, started_delta=0):
        """Fix jobs that are in a bad states
        :param in_queue_delta: lookup time in minutes for jobs
                                that are in enqueued state

        :param started_delta: lookup time in minutes for jobs
                                that are in enqueued state,
                                0 means that it is not checked
        """
        self._get_stuck_jobs_to_requeue(
            enqueued_delta=enqueued_delta,
            started_delta=started_delta
        ).requeue()
        return True

    @api.model
>>>>>>> upstream/12.0
=======
            enqueued_delta=enqueued_delta,
            started_delta=started_delta
        ).requeue()
        return True

    @api.model
>>>>>>> upstream/11.0
    def _get_stuck_jobs_domain(self, queue_dl, started_dl):
        domain = []
        now = fields.datetime.now()
        if queue_dl:
            queue_dl = now - timedelta(minutes=queue_dl)
<<<<<<< HEAD
<<<<<<< HEAD
            domain.append(
                [
                    "&",
                    ("date_enqueued", "<=", fields.Datetime.to_string(queue_dl)),
                    ("state", "=", "enqueued"),
                ]
            )
        if started_dl:
            started_dl = now - timedelta(minutes=started_dl)
            domain.append(
                [
                    "&",
                    ("date_started", "<=", fields.Datetime.to_string(started_dl)),
                    ("state", "=", "started"),
                ]
            )
=======
=======
>>>>>>> upstream/11.0
            domain.append([
                '&',
                ('date_enqueued', '<=', fields.Datetime.to_string(queue_dl)),
                ('state', '=', 'enqueued'),
            ])
        if started_dl:
            started_dl = now - timedelta(minutes=started_dl)
            domain.append([
                '&',
                ('date_started', '<=', fields.Datetime.to_string(started_dl)),
                ('state', '=', 'started'),
            ])
<<<<<<< HEAD
>>>>>>> upstream/12.0
=======
>>>>>>> upstream/11.0
        if not domain:
            raise exceptions.ValidationError(
                _("If both parameters are 0, ALL jobs will be requeued!")
            )
        return expression.OR(domain)

<<<<<<< HEAD
<<<<<<< HEAD
    def _get_stuck_jobs_to_requeue(self, enqueued_delta, started_delta):
        job_model = self.env["queue.job"]
        stuck_jobs = job_model.search(
            self._get_stuck_jobs_domain(enqueued_delta, started_delta)
        )
        return stuck_jobs

=======
=======
>>>>>>> upstream/11.0
    @api.model
    def _get_stuck_jobs_to_requeue(self, enqueued_delta, started_delta):
        job_model = self.env['queue.job']
        stuck_jobs = job_model.search(self._get_stuck_jobs_domain(
            enqueued_delta,
            started_delta,
        ))
        return stuck_jobs

    @api.multi
>>>>>>> upstream/12.0
    def related_action_open_record(self):
        """Open a form view with the record(s) of the job.

        For instance, for a job on a ``product.product``, it will open a
        ``product.product`` form view with the product record(s) concerned by
        the job. If the job concerns more than one record, it opens them in a
        list.

        This is the default related action.

        """
        self.ensure_one()
        records = self.records.exists()
        if not records:
            return None
        action = {
            "name": _("Related Record"),
            "type": "ir.actions.act_window",
            "view_mode": "form",
            "res_model": records._name,
        }
        if len(records) == 1:
            action["res_id"] = records.id
        else:
            action.update(
                {
                    "name": _("Related Records"),
                    "view_mode": "tree,form",
                    "domain": [("id", "in", records.ids)],
                }
            )
        return action

    def _test_job(self, failure_rate=0):
        _logger.info("Running test job.")
<<<<<<< HEAD
<<<<<<< HEAD
=======
        if random.random() <= failure_rate:
            raise JobError("Job failed")
>>>>>>> upstream/12.0


class RequeueJob(models.TransientModel):
    _name = "queue.requeue.job"
    _description = "Wizard to requeue a selection of jobs"

    def _default_job_ids(self):
        res = False
        context = self.env.context
        if context.get("active_model") == "queue.job" and context.get("active_ids"):
            res = context["active_ids"]
        return res

    job_ids = fields.Many2many(
        comodel_name="queue.job", string="Jobs", default=lambda r: r._default_job_ids()
    )

    def requeue(self):
        jobs = self.job_ids
        jobs.requeue()
        return {"type": "ir.actions.act_window_close"}


class SetJobsToDone(models.TransientModel):
    _inherit = "queue.requeue.job"
    _name = "queue.jobs.to.done"
    _description = "Set all selected jobs to done"

    def set_done(self):
        jobs = self.job_ids
        jobs.button_done()
        return {"type": "ir.actions.act_window_close"}


class SetJobsToCancelled(models.TransientModel):
    _inherit = 'queue.requeue.job'
    _name = 'queue.jobs.to.cancelled'
    _description = 'Cancel all selected jobs'

    @api.multi
    def set_cancelled(self):
        jobs = self.job_ids.filtered(
            lambda x: x.state in ('pending', 'failed', 'enqueued')
        )
        jobs.button_cancelled()
        return {'type': 'ir.actions.act_window_close'}


class SetJobsToDone(models.TransientModel):
    _inherit = 'queue.requeue.job'
    _name = 'queue.jobs.to.done'
    _description = 'Set all selected jobs to done'

    @api.multi
    def set_done(self):
        jobs = self.job_ids
        jobs.button_done()
        return {'type': 'ir.actions.act_window_close'}


class SetJobsToDone(models.TransientModel):
    _inherit = 'queue.requeue.job'
    _name = 'queue.jobs.to.done'
    _description = 'Set all selected jobs to done'

    reason = fields.Text(string='Reason to set to done')

    @api.multi
    def set_done(self):
        jobs = self.job_ids
        jobs.action_done(reason=self.reason)
        return {'type': 'ir.actions.act_window_close'}


class JobChannel(models.Model):
    _name = "queue.job.channel"
    _description = "Job Channels"

    name = fields.Char()
<<<<<<< HEAD
    complete_name = fields.Char(
        compute="_compute_complete_name", store=True, readonly=True
    )
    parent_id = fields.Many2one(
        comodel_name="queue.job.channel", string="Parent Channel", ondelete="restrict"
    )
    job_function_ids = fields.One2many(
        comodel_name="queue.job.function",
        inverse_name="channel_id",
        string="Job Functions",
    )
    removal_interval = fields.Integer(
        default=lambda self: self.env["queue.job"]._removal_interval, required=True
    )
=======
    complete_name = fields.Char(compute='_compute_complete_name',
                                store=True,
                                readonly=True)
    parent_id = fields.Many2one(comodel_name='queue.job.channel',
                                string='Parent Channel',
                                ondelete='restrict')
    job_function_ids = fields.One2many(comodel_name='queue.job.function',
                                       inverse_name='channel_id',
                                       string='Job Functions')
    removal_interval = fields.Integer(
        default=lambda self: self.env['queue.job']._removal_interval,
        required=True)
<<<<<<< HEAD
<<<<<<< HEAD
>>>>>>> upstream/12.0
=======
>>>>>>> upstream/11.0
=======
>>>>>>> upstream/10.0

    _sql_constraints = [
        ("name_uniq", "unique(complete_name)", "Channel complete name must be unique")
    ]

    @api.depends("name", "parent_id.complete_name")
    def _compute_complete_name(self):
        for record in self:
            if not record.name:
<<<<<<< HEAD
<<<<<<< HEAD
                complete_name = ""  # new record
            elif record.parent_id:
                complete_name = ".".join([record.parent_id.complete_name, record.name])
            else:
                complete_name = record.name
            record.complete_name = complete_name
=======
=======
>>>>>>> upstream/10.0
                continue  # new record
            channel = record
            parts = [channel.name]
            while channel.parent_id:
                channel = channel.parent_id
                parts.append(channel.name)
            record.complete_name = '.'.join(reversed(parts))
>>>>>>> upstream/11.0

    @api.constrains("parent_id", "name")
    def parent_required(self):
        for record in self:
            if record.name != "root" and not record.parent_id:
                raise exceptions.ValidationError(_("Parent channel required."))

    @api.model_create_multi
    def create(self, vals_list):
        records = self.browse()
        if self.env.context.get("install_mode"):
            # installing a module that creates a channel: rebinds the channel
            # to an existing one (likely we already had the channel created by
            # the @job decorator previously)
            new_vals_list = []
            for vals in vals_list:
                name = vals.get("name")
                parent_id = vals.get("parent_id")
                if name and parent_id:
                    existing = self.search(
                        [("name", "=", name), ("parent_id", "=", parent_id)]
                    )
                    if existing:
                        if not existing.get_metadata()[0].get("noupdate"):
                            existing.write(vals)
                        records |= existing
                        continue
                new_vals_list.append(vals)
            vals_list = new_vals_list
        records |= super().create(vals_list)
        return records

<<<<<<< HEAD
=======
    @api.model_create_multi
    def create(self, vals_list):
        records = self.browse()
        if self.env.context.get("install_mode"):
            # installing a module that creates a channel: rebinds the channel
            # to an existing one (likely we already had the channel created by
            # the @job decorator previously)
            new_vals_list = []
            for vals in vals_list:
                name = vals.get("name")
                parent_id = vals.get("parent_id")
                if name and parent_id:
                    existing = self.search(
                        [("name", "=", name), ("parent_id", "=", parent_id)]
                    )
                    if existing:
                        if not existing.get_metadata()[0].get("noupdate"):
                            existing.write(vals)
                        records |= existing
                        continue
                new_vals_list.append(vals)
            vals_list = new_vals_list
        records |= super().create(vals_list)
        return records

    @api.multi
>>>>>>> upstream/12.0
    def write(self, values):
        for channel in self:
            if (
                not self.env.context.get("install_mode")
                and channel.name == "root"
                and ("name" in values or "parent_id" in values)
            ):
                raise exceptions.UserError(_("Cannot change the root channel"))
        return super(JobChannel, self).write(values)

    def unlink(self):
        for channel in self:
            if channel.name == "root":
                raise exceptions.UserError(_("Cannot remove the root channel"))
        return super(JobChannel, self).unlink()

    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, record.complete_name))
        return result


class JobFunction(models.Model):
    _name = "queue.job.function"
    _description = "Job Functions"
    _log_access = False

    JobConfig = namedtuple(
        "JobConfig",
        "channel "
        "retry_pattern "
        "related_action_enable "
        "related_action_func_name "
        "related_action_kwargs "
        "job_function_id ",
    )

<<<<<<< HEAD
    def _default_channel(self):
        return self.env.ref("queue_job.channel_root")
=======
    @api.model
    def _default_channel(self):
        return self.env.ref('queue_job.channel_root')
>>>>>>> upstream/12.0

    name = fields.Char(
        compute="_compute_name", inverse="_inverse_name", index=True, store=True,
    )

    # model and method should be required, but the required flag doesn't
    # let a chance to _inverse_name to be executed
    model_id = fields.Many2one(
        comodel_name="ir.model", string="Model", ondelete="cascade"
    )
    method = fields.Char()

<<<<<<< HEAD
    channel_id = fields.Many2one(
        comodel_name="queue.job.channel",
        string="Channel",
        required=True,
        default=lambda r: r._default_channel(),
    )
    channel = fields.Char(related="channel_id.complete_name", store=True, readonly=True)
=======
    channel_id = fields.Many2one(comodel_name='queue.job.channel',
                                 string='Channel',
                                 required=True,
                                 default=_default_channel)
    channel = fields.Char(related='channel_id.complete_name',
                          store=True,
                          readonly=True)
>>>>>>> upstream/12.0
    retry_pattern = JobSerialized(string="Retry Pattern (serialized)", base_type=dict)
    edit_retry_pattern = fields.Text(
        string="Retry Pattern",
        compute="_compute_edit_retry_pattern",
        inverse="_inverse_edit_retry_pattern",
        help="Pattern expressing from the count of retries on retryable errors,"
        " the number of of seconds to postpone the next execution.\n"
        "Example: {1: 10, 5: 20, 10: 30, 15: 300}.\n"
        "See the module description for details.",
    )
    related_action = JobSerialized(string="Related Action (serialized)", base_type=dict)
    edit_related_action = fields.Text(
        string="Related Action",
        compute="_compute_edit_related_action",
        inverse="_inverse_edit_related_action",
        help="The action when the button *Related Action* is used on a job. "
        "The default action is to open the view of the record related "
        "to the job. Configured as a dictionary with optional keys: "
        "enable, func_name, kwargs.\n"
        "See the module description for details.",
    )

    @api.depends("model_id.model", "method")
    def _compute_name(self):
        for record in self:
            if not (record.model_id and record.method):
                record.name = ""
                continue
            record.name = self.job_function_name(record.model_id.model, record.method)

    def _inverse_name(self):
        groups = regex_job_function_name.match(self.name)
        if not groups:
            raise exceptions.UserError(_("Invalid job function: {}").format(self.name))
<<<<<<< HEAD
        model_name = groups[1]
        method = groups[2]
=======
        model_name = groups.group(1)
        method = groups.group(2)
>>>>>>> upstream/12.0
        model = self.env["ir.model"].search([("model", "=", model_name)], limit=1)
        if not model:
            raise exceptions.UserError(_("Model {} not found").format(model_name))
        self.model_id = model.id
        self.method = method

    @api.depends("retry_pattern")
    def _compute_edit_retry_pattern(self):
        for record in self:
            retry_pattern = record._parse_retry_pattern()
            record.edit_retry_pattern = str(retry_pattern)

    def _inverse_edit_retry_pattern(self):
        try:
            self.retry_pattern = ast.literal_eval(self.edit_retry_pattern or "{}")
        except (ValueError, TypeError):
            raise exceptions.UserError(self._retry_pattern_format_error_message())

    @api.depends("related_action")
    def _compute_edit_related_action(self):
        for record in self:
            record.edit_related_action = str(record.related_action)

    def _inverse_edit_related_action(self):
        try:
            self.related_action = ast.literal_eval(self.edit_related_action or "{}")
        except (ValueError, TypeError):
            raise exceptions.UserError(self._related_action_format_error_message())

    @staticmethod
    def job_function_name(model_name, method_name):
        return "<{}>.{}".format(model_name, method_name)

    # TODO deprecated by :job-no-decorator:
<<<<<<< HEAD
=======
    @api.model
>>>>>>> upstream/12.0
    def _find_or_create_channel(self, channel_path):
        channel_model = self.env["queue.job.channel"]
        parts = channel_path.split(".")
        parts.reverse()
        channel_name = parts.pop()
        assert channel_name == "root", "A channel path starts with 'root'"
        # get the root channel
        channel = channel_model.search([("name", "=", channel_name)])
        while parts:
            channel_name = parts.pop()
            parent_channel = channel
<<<<<<< HEAD
            channel = channel_model.search(
                [("name", "=", channel_name), ("parent_id", "=", parent_channel.id)],
                limit=1,
            )
=======
            channel = channel_model.search([
                ('name', '=', channel_name),
                ('parent_id', '=', parent_channel.id),
            ], limit=1)
>>>>>>> upstream/11.0
            if not channel:
                channel = channel_model.create(
                    {"name": channel_name, "parent_id": parent_channel.id}
                )
        return channel

    def job_default_config(self):
        return self.JobConfig(
            channel="root",
            retry_pattern={},
            related_action_enable=True,
            related_action_func_name=None,
            related_action_kwargs={},
            job_function_id=None,
        )

    def _parse_retry_pattern(self):
        try:
            # as json can't have integers as keys and the field is stored
            # as json, convert back to int
            retry_pattern = {
                int(try_count): postpone_seconds
                for try_count, postpone_seconds in self.retry_pattern.items()
            }
        except ValueError:
            _logger.error(
                "Invalid retry pattern for job function %s,"
                " keys could not be parsed as integers, fallback"
                " to the default retry pattern.",
                self.name,
            )
            retry_pattern = {}
        return retry_pattern

    @tools.ormcache("name")
    def job_config(self, name):
        config = self.search([("name", "=", name)], limit=1)
        if not config:
            return self.job_default_config()
        retry_pattern = config._parse_retry_pattern()
        return self.JobConfig(
            channel=config.channel,
            retry_pattern=retry_pattern,
            related_action_enable=config.related_action.get("enable", True),
            related_action_func_name=config.related_action.get("func_name"),
            related_action_kwargs=config.related_action.get("kwargs"),
            job_function_id=config.id,
        )

    def _retry_pattern_format_error_message(self):
        return _(
            "Unexpected format of Retry Pattern for {}.\n"
            "Example of valid format:\n"
            "{{1: 300, 5: 600, 10: 1200, 15: 3000}}"
        ).format(self.name)

    @api.constrains("retry_pattern")
    def _check_retry_pattern(self):
        for record in self:
            retry_pattern = record.retry_pattern
            if not retry_pattern:
                continue

            all_values = list(retry_pattern) + list(retry_pattern.values())
            for value in all_values:
                try:
                    int(value)
                except ValueError:
                    raise exceptions.UserError(
                        record._retry_pattern_format_error_message()
                    )

    def _related_action_format_error_message(self):
        return _(
            "Unexpected format of Related Action for {}.\n"
            "Example of valid format:\n"
            '{{"enable": True, "func_name": "related_action_foo",'
            ' "kwargs" {{"limit": 10}}}}'
        ).format(self.name)

    @api.constrains("related_action")
    def _check_related_action(self):
        valid_keys = ("enable", "func_name", "kwargs")
        for record in self:
            related_action = record.related_action
            if not related_action:
                continue

            if any(key not in valid_keys for key in related_action):
                raise exceptions.UserError(
                    record._related_action_format_error_message()
                )

    @api.model_create_multi
    def create(self, vals_list):
        records = self.browse()
        if self.env.context.get("install_mode"):
            # installing a module that creates a job function: rebinds the record
            # to an existing one (likely we already had the job function created by
            # the @job decorator previously)
            new_vals_list = []
            for vals in vals_list:
                name = vals.get("name")
                if name:
                    existing = self.search([("name", "=", name)], limit=1)
                    if existing:
                        if not existing.get_metadata()[0].get("noupdate"):
                            existing.write(vals)
                        records |= existing
                        continue
                new_vals_list.append(vals)
            vals_list = new_vals_list
        records |= super().create(vals_list)
        self.clear_caches()
        return records

    def write(self, values):
        res = super().write(values)
        self.clear_caches()
        return res

    def unlink(self):
        res = super().unlink()
        self.clear_caches()
        return res

    # TODO deprecated by :job-no-decorator:
    def _register_job(self, model, job_method):
        func_name = self.job_function_name(model._name, job_method.__name__)
<<<<<<< HEAD
        if not self.search_count([("name", "=", func_name)]):
=======
        if not self.search_count([('name', '=', func_name)]):
>>>>>>> upstream/12.0
            channel = self._find_or_create_channel(job_method.default_channel)
            self.create({"name": func_name, "channel_id": channel.id})
=======
        if random.random() <= failure_rate:
            raise JobError("Job failed")
>>>>>>> upstream/14.0
