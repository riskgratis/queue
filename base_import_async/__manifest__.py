# @author Stéphane Bidoul <stephane.bidoul@acsone.eu>
# @author Sébastien BEAU <sebastien.beau@akretion.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
<<<<<<< HEAD
    "name": "Asynchronous Import",
    "summary": "Import CSV files in the background",
    "version": "14.0.1.0.2",
    "author": "Akretion, ACSONE SA/NV, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "website": "https://github.com/OCA/queue",
    "category": "Generic Modules",
    "depends": ["base_import", "queue_job"],
    "data": ["data/queue_job_function_data.xml", "views/base_import_async.xml"],
    "qweb": ["static/src/xml/import.xml"],
    "installable": True,
    "development_status": "Production/Stable",
=======
    'name': 'Asynchronous Import',
    'summary': 'Import CSV files in the background',
<<<<<<< HEAD
    'version': '12.0.2.0.0',
=======
    'version': '11.0.2.0.1',
>>>>>>> upstream/11.0
    'author': 'Akretion, ACSONE SA/NV, Odoo Community Association (OCA)',
    'license': 'AGPL-3',
    'website': 'https://github.com/OCA/queue',
    'category': 'Generic Modules',
    'depends': [
        'base_import',
        'queue_job',
        'queue_job_batch',
    ],
    'data': [
        'views/base_import_async.xml',
    ],
    'qweb': [
        'static/src/xml/import.xml',
    ],
    'installable': True,
    'development_status': 'Production/Stable',
>>>>>>> upstream/12.0
}
