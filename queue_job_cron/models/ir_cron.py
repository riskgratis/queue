<<<<<<< HEAD
# Copyright 2019 ACSONE SA/NV (<http://acsone.eu>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)
import logging
<<<<<<< HEAD

from odoo import api, fields, models
=======
from odoo import api, fields, models
from odoo.addons.queue_job.job import job
>>>>>>> upstream/11.0
=======
# -*- coding: utf-8 -*-
# Copyright 2017 ACSONE SA/NV (<http://acsone.eu>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)
import logging
from odoo import api, fields, models, _
from odoo.addons.queue_job.job import job
from odoo.exceptions import ValidationError
from odoo.addons.base.ir.ir_cron import str2tuple
>>>>>>> upstream/10.0

_logger = logging.getLogger(__name__)


class IrCron(models.Model):
<<<<<<< HEAD
<<<<<<< HEAD
    _inherit = "ir.cron"

    run_as_queue_job = fields.Boolean(
        help="Specify if this cron should be ran as a queue job"
    )
    channel_id = fields.Many2one(
        comodel_name="queue.job.channel",
        compute="_compute_run_as_queue_job",
        readonly=False,
        store=True,
        string="Channel",
    )

    @api.depends("run_as_queue_job")
    def _compute_run_as_queue_job(self):
        for cron in self:
            if cron.channel_id:
                continue
            if cron.run_as_queue_job:
                cron.channel_id = self.env.ref("queue_job_cron.channel_root_ir_cron").id
            else:
                cron.channel_id = False

    def _run_job_as_queue_job(self, server_action):
        return server_action.run()

    def method_direct_trigger(self):
<<<<<<< HEAD
        for cron in self:
            if not cron.run_as_queue_job:
                super(IrCron, cron).method_direct_trigger()
            else:
                _cron = cron.with_user(cron.user_id).with_context(
                    lastcall=cron.lastcall
                )
                _cron.with_delay(
                    priority=_cron.priority,
                    description=_cron.name,
                    channel=_cron.channel_id.complete_name,
                )._run_job_as_queue_job(server_action=_cron.ir_actions_server_id)
        return True
=======
=======
=======
>>>>>>> upstream/10.0
    _inherit = 'ir.cron'

    run_as_queue_job = fields.Boolean(help="Specify if this cron should be "
                                           "ran as a queue job")
    channel_id = fields.Many2one(comodel_name='queue.job.channel',
                                 string='Channel')

    @api.onchange('run_as_queue_job')
    def onchange_run_as_queue_job(self):
        for cron in self:
            if cron.run_as_queue_job and not cron.channel_id:
                cron.channel_id = self.env.ref(
                    'queue_job_cron.channel_root_ir_cron').id

    @job(default_channel='root.ir_cron')
    @api.model
<<<<<<< HEAD
    def _run_job_as_queue_job(self, server_action):
        return server_action.run()

    @api.multi
    def method_direct_trigger(self):
>>>>>>> upstream/11.0
        self.check_access_rights('write')
        if self.run_as_queue_job:
            return self.sudo(user=self.user_id.id).with_delay(
                priority=self.priority,
                description=self.name,
                channel=self.channel_id.complete_name)._run_job_as_queue_job(
                server_action=self.ir_actions_server_id.sudo(self.user_id.id))
        else:
            return super(IrCron, self).method_direct_trigger()
<<<<<<< HEAD
>>>>>>> upstream/12.0

    def _callback(self, cron_name, server_action_id, job_id):
        cron = self.env["ir.cron"].sudo().browse(job_id)
        if cron.run_as_queue_job:
            server_action = self.env["ir.actions.server"].browse(server_action_id)
            return self.with_delay(
                priority=cron.priority,
                description=cron.name,
<<<<<<< HEAD
                channel=cron.channel_id.complete_name,
            )._run_job_as_queue_job(server_action=server_action)
=======
                channel=cron.channel_id.complete_name)._run_job_as_queue_job(
                    server_action=server_action)
>>>>>>> upstream/12.0
        else:
            return super()._callback(
                cron_name=cron_name, server_action_id=server_action_id, job_id=job_id
            )
=======

    @api.model
    def _callback(self, cron_name, server_action_id, job_id):
        cron = self.env['ir.cron'].sudo().browse(job_id)
        if cron.run_as_queue_job:
            server_action = self.env['ir.actions.server'].browse(
                server_action_id)
=======
    def _run_job_as_queue_job(self, model_name, method_name, args):
        args = str2tuple(args)
        if model_name in self.env:
            model = self.env[model_name]
            if hasattr(model, method_name):
                return getattr(model, method_name)(*args)
            else:
                raise ValidationError(_("Method '%s.%s' does not exist." %
                                        (model_name, method_name)))
        else:
            raise ValidationError(_("Model %r does not exist." % model_name))

    @api.model
    def _callback(self, model_name, method_name, args, job_id):
        cron = self.env['ir.cron'].sudo().browse(job_id)
        if cron.run_as_queue_job:
>>>>>>> upstream/10.0
            return self.with_delay(
                priority=cron.priority,
                description=cron.name,
                channel=cron.channel_id.complete_name)._run_job_as_queue_job(
<<<<<<< HEAD
                    server_action=server_action)
        else:
            return super(IrCron, self)._callback(
                cron_name=cron_name,
                server_action_id=server_action_id,
                job_id=job_id)
>>>>>>> upstream/11.0
=======
                    model_name=model_name,
                    method_name=method_name,
                    args=args)
        else:
            return super(IrCron, self)._callback(
                model_name=model_name,
                method_name=method_name,
                args=args,
                job_id=job_id)
>>>>>>> upstream/10.0
